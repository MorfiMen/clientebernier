﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Threading.Tasks;
using Cliente.Models;
using CsvHelper;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RestSharp;

namespace Cliente.Controllers
{
    public class PrediccionesController : Controller
    {

        public IActionResult Index()
        {
            
            
            return View();
        }

        public IActionResult Predicciones()
        {
            return View();
        }

        public string GetReleases(string url)
        {
            var client = new RestClient(url);

            var response = client.Execute(new RestRequest());

            return response.Content;
        }

        public ContentResult JSON()
        {
            String json_predictions = GetReleases("http://modelobernier.azurewebsites.net/");

            var dato = JsonConvert.DeserializeObject<List<Data>>(json_predictions);

            List<DataPoint> dataPoints = new List<DataPoint>();
            for (var i = 0; i < 30; i++)
            {
                dataPoints.Add(new DataPoint(dato[i].Date, dato[i].RRM));
            }
            JsonSerializerSettings _jsonSetting = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
            return Content(JsonConvert.SerializeObject(dataPoints, _jsonSetting), "application/json");
        }

    }

}