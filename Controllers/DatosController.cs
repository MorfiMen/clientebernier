﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Threading.Tasks;
using Cliente.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RestSharp;

namespace Cliente.Controllers
{
    public class DatosController : Controller
    {
        public IActionResult Index()
        {
            
            return View();
        }

        public IActionResult Datos()
        {
            return null;
        }

        public string GetReleases(string url)
        {
            var client = new RestClient(url);

            var response = client.Execute(new RestRequest());

            return response.Content;
        }

        public ContentResult JSON_RRM()
        {
            String json_datos = GetReleases("http://modelobernier.azurewebsites.net/datos");

            var dato = JsonConvert.DeserializeObject<List<Data>>(json_datos);

            List<DataPoint> dataPoints = new List<DataPoint>();
            for (var i = 0; i < 4385; i++)
            {
                dataPoints.Add(new DataPoint(dato[i].Date, dato[i].RRM));
            }
            JsonSerializerSettings _jsonSetting = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
            return Content(JsonConvert.SerializeObject(dataPoints, _jsonSetting), "application/json");
        }

        public ContentResult JSON_CPI()
        {
            String json_datos = GetReleases("http://modelobernier.azurewebsites.net/datos");

            var dato = JsonConvert.DeserializeObject<List<Data>>(json_datos);

            List<DataPoint> dataPoints = new List<DataPoint>();
            for (var i = 0; i < 4385; i++)
            {
                dataPoints.Add(new DataPoint(dato[i].Date, dato[i].CPI));
            }
            JsonSerializerSettings _jsonSetting = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
            return Content(JsonConvert.SerializeObject(dataPoints, _jsonSetting), "application/json");
        }

        public ContentResult JSON_INFLATION()
        {
            String json_datos = GetReleases("http://modelobernier.azurewebsites.net/datos");

            var dato = JsonConvert.DeserializeObject<List<Data>>(json_datos);

            List<DataPoint> dataPoints = new List<DataPoint>();
            for (var i = 0; i < 4385; i++)
            {
                dataPoints.Add(new DataPoint(dato[i].Date, dato[i].Inflation));
            }
            JsonSerializerSettings _jsonSetting = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
            return Content(JsonConvert.SerializeObject(dataPoints, _jsonSetting), "application/json");
        }

        public ContentResult JSON_PPI()
        {
            String json_datos = GetReleases("http://modelobernier.azurewebsites.net/datos");

            var dato = JsonConvert.DeserializeObject<List<Data>>(json_datos);

            List<DataPoint> dataPoints = new List<DataPoint>();
            for (var i = 0; i < 4385; i++)
            {
                dataPoints.Add(new DataPoint(dato[i].Date, dato[i].PPI*100));
            }
            JsonSerializerSettings _jsonSetting = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
            return Content(JsonConvert.SerializeObject(dataPoints, _jsonSetting), "application/json");
        }

        public ContentResult JSON_EXTERNAL()
        {
            String json_datos = GetReleases("http://modelobernier.azurewebsites.net/datos");

            var dato = JsonConvert.DeserializeObject<List<Data>>(json_datos);

            List<DataPoint> dataPoints = new List<DataPoint>();
            for (var i = 0; i < 4385; i++)
            {
                dataPoints.Add(new DataPoint(dato[i].Date, dato[i].External_debt));
            }
            JsonSerializerSettings _jsonSetting = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
            return Content(JsonConvert.SerializeObject(dataPoints, _jsonSetting), "application/json");
        }

    }
}