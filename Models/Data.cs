﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Cliente.Models
{

    
    
    public class Data
    {
        public string Date { get; set; }
        public float Inflation { get; set; }
        public float CPI { get; set; }
        public float PPI { get; set; }
        public float External_debt { get; set; }
        public float RRM { get; set; }

    }
}
